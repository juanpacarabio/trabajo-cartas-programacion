﻿namespace JuegoDeCartas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.button8 = new System.Windows.Forms.Button();
            this.listadeimagenes = new System.Windows.Forms.ImageList(this.components);
            this.fotocarta = new System.Windows.Forms.Label();
            this.btn_lista = new System.Windows.Forms.Button();
            this.valor_carta_j1 = new System.Windows.Forms.TextBox();
            this.btn_c2_j1 = new System.Windows.Forms.Button();
            this.btn_c3_j1 = new System.Windows.Forms.Button();
            this.btn_c1_j2 = new System.Windows.Forms.Button();
            this.btn_c2_j2 = new System.Windows.Forms.Button();
            this.btn_c3_j2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_mazo_j1 = new System.Windows.Forms.Button();
            this.btn_mazo_j2 = new System.Windows.Forms.Button();
            this.btn_mazo_empate = new System.Windows.Forms.Button();
            this.btn_ver_c1_j1 = new System.Windows.Forms.Button();
            this.btn_ver_c2_j1 = new System.Windows.Forms.Button();
            this.btn_ver_c3_j1 = new System.Windows.Forms.Button();
            this.btn_ver_c1_j2 = new System.Windows.Forms.Button();
            this.btn_ver_c2_j2 = new System.Windows.Forms.Button();
            this.btn_ver_c3_j2 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.GhostWhite;
            this.label1.Location = new System.Drawing.Point(55, 254);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Cartas Restantes: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.GhostWhite;
            this.label2.Location = new System.Drawing.Point(155, 254);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "00";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox3.Location = new System.Drawing.Point(448, 41);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(71, 113);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox4.Location = new System.Drawing.Point(563, 41);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(71, 113);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox5.Location = new System.Drawing.Point(675, 41);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(71, 113);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 7;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox6.Location = new System.Drawing.Point(448, 299);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(71, 113);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 8;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox7.Location = new System.Drawing.Point(563, 299);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(71, 113);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 9;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pictureBox7_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox8.Location = new System.Drawing.Point(675, 299);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(71, 113);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 10;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox8_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox9.Location = new System.Drawing.Point(634, 187);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(55, 80);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 17;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox10.Location = new System.Drawing.Point(508, 187);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(55, 80);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 18;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Enabled = false;
            this.pictureBox11.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox11.Location = new System.Drawing.Point(333, 312);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(42, 75);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 19;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::JuegoDeCartas.Properties.Resources.DorsoCartas;
            this.pictureBox12.Location = new System.Drawing.Point(333, 61);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(42, 82);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 20;
            this.pictureBox12.TabStop = false;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(459, 414);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(48, 23);
            this.button8.TabIndex = 23;
            this.button8.Text = "Pedir";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // listadeimagenes
            // 
            this.listadeimagenes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("listadeimagenes.ImageStream")));
            this.listadeimagenes.TransparentColor = System.Drawing.Color.Transparent;
            this.listadeimagenes.Images.SetKeyName(0, "DorsoCartas.png");
            this.listadeimagenes.Images.SetKeyName(1, "BA_1.png");
            this.listadeimagenes.Images.SetKeyName(2, "BA_2.png");
            this.listadeimagenes.Images.SetKeyName(3, "BA_3.png");
            this.listadeimagenes.Images.SetKeyName(4, "BA_4.png");
            this.listadeimagenes.Images.SetKeyName(5, "BA_5.png");
            this.listadeimagenes.Images.SetKeyName(6, "BA_6.png");
            this.listadeimagenes.Images.SetKeyName(7, "BA_7.png");
            this.listadeimagenes.Images.SetKeyName(8, "BA_8.png");
            this.listadeimagenes.Images.SetKeyName(9, "BA_9.png");
            this.listadeimagenes.Images.SetKeyName(10, "BA_10.png");
            this.listadeimagenes.Images.SetKeyName(11, "BA_11.png");
            this.listadeimagenes.Images.SetKeyName(12, "BA_12.png");
            this.listadeimagenes.Images.SetKeyName(13, "CO_1.png");
            this.listadeimagenes.Images.SetKeyName(14, "CO_2.png");
            this.listadeimagenes.Images.SetKeyName(15, "CO_3.png");
            this.listadeimagenes.Images.SetKeyName(16, "CO_4.png");
            this.listadeimagenes.Images.SetKeyName(17, "CO_5.png");
            this.listadeimagenes.Images.SetKeyName(18, "CO_6.png");
            this.listadeimagenes.Images.SetKeyName(19, "CO_7.png");
            this.listadeimagenes.Images.SetKeyName(20, "CO_8.png");
            this.listadeimagenes.Images.SetKeyName(21, "CO_9.png");
            this.listadeimagenes.Images.SetKeyName(22, "CO_10.png");
            this.listadeimagenes.Images.SetKeyName(23, "CO_11.png");
            this.listadeimagenes.Images.SetKeyName(24, "CO_12.png");
            this.listadeimagenes.Images.SetKeyName(25, "ES_1.png");
            this.listadeimagenes.Images.SetKeyName(26, "ES_2.png");
            this.listadeimagenes.Images.SetKeyName(27, "ES_3.png");
            this.listadeimagenes.Images.SetKeyName(28, "ES_4.png");
            this.listadeimagenes.Images.SetKeyName(29, "ES_5.png");
            this.listadeimagenes.Images.SetKeyName(30, "ES_6.png");
            this.listadeimagenes.Images.SetKeyName(31, "ES_7.png");
            this.listadeimagenes.Images.SetKeyName(32, "ES_8.png");
            this.listadeimagenes.Images.SetKeyName(33, "ES_9.png");
            this.listadeimagenes.Images.SetKeyName(34, "ES_10.png");
            this.listadeimagenes.Images.SetKeyName(35, "ES_11.png");
            this.listadeimagenes.Images.SetKeyName(36, "ES_12.png");
            this.listadeimagenes.Images.SetKeyName(37, "OR_1.png");
            this.listadeimagenes.Images.SetKeyName(38, "OR_2.png");
            this.listadeimagenes.Images.SetKeyName(39, "OR_3.png");
            this.listadeimagenes.Images.SetKeyName(40, "OR_4.png");
            this.listadeimagenes.Images.SetKeyName(41, "OR_5.png");
            this.listadeimagenes.Images.SetKeyName(42, "OR_6.png");
            this.listadeimagenes.Images.SetKeyName(43, "OR_7.png");
            this.listadeimagenes.Images.SetKeyName(44, "OR_8.png");
            this.listadeimagenes.Images.SetKeyName(45, "OR_10.png");
            this.listadeimagenes.Images.SetKeyName(46, "OR_11.png");
            this.listadeimagenes.Images.SetKeyName(47, "OR_12.png");
            // 
            // fotocarta
            // 
            this.fotocarta.ImageIndex = 0;
            this.fotocarta.ImageList = this.listadeimagenes;
            this.fotocarta.Location = new System.Drawing.Point(93, 72);
            this.fotocarta.Name = "fotocarta";
            this.fotocarta.Size = new System.Drawing.Size(103, 141);
            this.fotocarta.TabIndex = 24;
            // 
            // btn_lista
            // 
            this.btn_lista.Location = new System.Drawing.Point(48, 312);
            this.btn_lista.Name = "btn_lista";
            this.btn_lista.Size = new System.Drawing.Size(202, 40);
            this.btn_lista.TabIndex = 25;
            this.btn_lista.Text = "Ver Carta";
            this.btn_lista.UseVisualStyleBackColor = true;
            this.btn_lista.Click += new System.EventHandler(this.btn_lista_Click);
            // 
            // valor_carta_j1
            // 
            this.valor_carta_j1.Location = new System.Drawing.Point(58, 225);
            this.valor_carta_j1.Name = "valor_carta_j1";
            this.valor_carta_j1.Size = new System.Drawing.Size(176, 20);
            this.valor_carta_j1.TabIndex = 26;
            // 
            // btn_c2_j1
            // 
            this.btn_c2_j1.Location = new System.Drawing.Point(575, 414);
            this.btn_c2_j1.Name = "btn_c2_j1";
            this.btn_c2_j1.Size = new System.Drawing.Size(48, 23);
            this.btn_c2_j1.TabIndex = 27;
            this.btn_c2_j1.Text = "Pedir";
            this.btn_c2_j1.UseVisualStyleBackColor = true;
            this.btn_c2_j1.Click += new System.EventHandler(this.btn_c2_j1_Click);
            // 
            // btn_c3_j1
            // 
            this.btn_c3_j1.Location = new System.Drawing.Point(685, 414);
            this.btn_c3_j1.Name = "btn_c3_j1";
            this.btn_c3_j1.Size = new System.Drawing.Size(48, 23);
            this.btn_c3_j1.TabIndex = 28;
            this.btn_c3_j1.Text = "Pedir";
            this.btn_c3_j1.UseVisualStyleBackColor = true;
            this.btn_c3_j1.Click += new System.EventHandler(this.btn_c3_j1_Click);
            // 
            // btn_c1_j2
            // 
            this.btn_c1_j2.Location = new System.Drawing.Point(459, 12);
            this.btn_c1_j2.Name = "btn_c1_j2";
            this.btn_c1_j2.Size = new System.Drawing.Size(48, 23);
            this.btn_c1_j2.TabIndex = 29;
            this.btn_c1_j2.Text = "Pedir";
            this.btn_c1_j2.UseVisualStyleBackColor = true;
            this.btn_c1_j2.Click += new System.EventHandler(this.btn_c1_j2_Click);
            // 
            // btn_c2_j2
            // 
            this.btn_c2_j2.Location = new System.Drawing.Point(575, 12);
            this.btn_c2_j2.Name = "btn_c2_j2";
            this.btn_c2_j2.Size = new System.Drawing.Size(48, 23);
            this.btn_c2_j2.TabIndex = 30;
            this.btn_c2_j2.Text = "Pedir";
            this.btn_c2_j2.UseVisualStyleBackColor = true;
            this.btn_c2_j2.Click += new System.EventHandler(this.btn_c2_j2_Click);
            // 
            // btn_c3_j2
            // 
            this.btn_c3_j2.Location = new System.Drawing.Point(685, 12);
            this.btn_c3_j2.Name = "btn_c3_j2";
            this.btn_c3_j2.Size = new System.Drawing.Size(48, 23);
            this.btn_c3_j2.TabIndex = 31;
            this.btn_c3_j2.Text = "Pedir";
            this.btn_c3_j2.UseVisualStyleBackColor = true;
            this.btn_c3_j2.Click += new System.EventHandler(this.btn_c3_j2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(445, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Carta J1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(705, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Carta J2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(334, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 39;
            this.label5.Text = "Jugador 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(349, 419);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Jugador 1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(25, 414);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Juego de Cartas";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(271, 390);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Cartas Acumuladas: 0 ";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(271, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "Cartas Acumuladas:  0";
            // 
            // btn_mazo_j1
            // 
            this.btn_mazo_j1.Location = new System.Drawing.Point(309, 258);
            this.btn_mazo_j1.Name = "btn_mazo_j1";
            this.btn_mazo_j1.Size = new System.Drawing.Size(94, 24);
            this.btn_mazo_j1.TabIndex = 44;
            this.btn_mazo_j1.Text = "A Mazo J1";
            this.btn_mazo_j1.UseVisualStyleBackColor = true;
            this.btn_mazo_j1.Click += new System.EventHandler(this.btn_mazo_j1_Click);
            // 
            // btn_mazo_j2
            // 
            this.btn_mazo_j2.Location = new System.Drawing.Point(309, 170);
            this.btn_mazo_j2.Name = "btn_mazo_j2";
            this.btn_mazo_j2.Size = new System.Drawing.Size(94, 24);
            this.btn_mazo_j2.TabIndex = 45;
            this.btn_mazo_j2.Text = "A Mazo J2";
            this.btn_mazo_j2.UseVisualStyleBackColor = true;
            this.btn_mazo_j2.Click += new System.EventHandler(this.btn_mazo_j2_Click);
            // 
            // btn_mazo_empate
            // 
            this.btn_mazo_empate.Location = new System.Drawing.Point(309, 214);
            this.btn_mazo_empate.Name = "btn_mazo_empate";
            this.btn_mazo_empate.Size = new System.Drawing.Size(94, 24);
            this.btn_mazo_empate.TabIndex = 46;
            this.btn_mazo_empate.Text = "Empate";
            this.btn_mazo_empate.UseVisualStyleBackColor = true;
            this.btn_mazo_empate.Click += new System.EventHandler(this.btn_mazo_empate_Click);
            // 
            // btn_ver_c1_j1
            // 
            this.btn_ver_c1_j1.Location = new System.Drawing.Point(39, 81);
            this.btn_ver_c1_j1.Name = "btn_ver_c1_j1";
            this.btn_ver_c1_j1.Size = new System.Drawing.Size(35, 24);
            this.btn_ver_c1_j1.TabIndex = 48;
            this.btn_ver_c1_j1.Text = "C1";
            this.btn_ver_c1_j1.UseVisualStyleBackColor = true;
            this.btn_ver_c1_j1.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // btn_ver_c2_j1
            // 
            this.btn_ver_c2_j1.Location = new System.Drawing.Point(39, 119);
            this.btn_ver_c2_j1.Name = "btn_ver_c2_j1";
            this.btn_ver_c2_j1.Size = new System.Drawing.Size(35, 24);
            this.btn_ver_c2_j1.TabIndex = 49;
            this.btn_ver_c2_j1.Text = "C2";
            this.btn_ver_c2_j1.UseVisualStyleBackColor = true;
            this.btn_ver_c2_j1.Click += new System.EventHandler(this.btn_ver_c2_j1_Click);
            // 
            // btn_ver_c3_j1
            // 
            this.btn_ver_c3_j1.Location = new System.Drawing.Point(39, 158);
            this.btn_ver_c3_j1.Name = "btn_ver_c3_j1";
            this.btn_ver_c3_j1.Size = new System.Drawing.Size(35, 24);
            this.btn_ver_c3_j1.TabIndex = 50;
            this.btn_ver_c3_j1.Text = "C3";
            this.btn_ver_c3_j1.UseVisualStyleBackColor = true;
            this.btn_ver_c3_j1.Click += new System.EventHandler(this.btn_ver_c3_j1_Click);
            // 
            // btn_ver_c1_j2
            // 
            this.btn_ver_c1_j2.Location = new System.Drawing.Point(215, 81);
            this.btn_ver_c1_j2.Name = "btn_ver_c1_j2";
            this.btn_ver_c1_j2.Size = new System.Drawing.Size(35, 24);
            this.btn_ver_c1_j2.TabIndex = 51;
            this.btn_ver_c1_j2.Text = "C1";
            this.btn_ver_c1_j2.UseVisualStyleBackColor = true;
            this.btn_ver_c1_j2.Click += new System.EventHandler(this.btn_ver_c1_j2_Click);
            // 
            // btn_ver_c2_j2
            // 
            this.btn_ver_c2_j2.Location = new System.Drawing.Point(215, 119);
            this.btn_ver_c2_j2.Name = "btn_ver_c2_j2";
            this.btn_ver_c2_j2.Size = new System.Drawing.Size(35, 24);
            this.btn_ver_c2_j2.TabIndex = 52;
            this.btn_ver_c2_j2.Text = "C2";
            this.btn_ver_c2_j2.UseVisualStyleBackColor = true;
            this.btn_ver_c2_j2.Click += new System.EventHandler(this.btn_ver_c2_j2_Click);
            // 
            // btn_ver_c3_j2
            // 
            this.btn_ver_c3_j2.Location = new System.Drawing.Point(215, 158);
            this.btn_ver_c3_j2.Name = "btn_ver_c3_j2";
            this.btn_ver_c3_j2.Size = new System.Drawing.Size(35, 24);
            this.btn_ver_c3_j2.TabIndex = 53;
            this.btn_ver_c3_j2.Text = "C3";
            this.btn_ver_c3_j2.UseVisualStyleBackColor = true;
            this.btn_ver_c3_j2.Click += new System.EventHandler(this.btn_ver_c3_j2_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(93, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 54;
            this.label10.Text = "Visualizador de Carta";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(433, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 13);
            this.label11.TabIndex = 55;
            this.label11.Text = "C1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(549, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 13);
            this.label12.TabIndex = 56;
            this.label12.Text = "C2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(659, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 13);
            this.label13.TabIndex = 57;
            this.label13.Text = "C3";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(433, 424);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 58;
            this.label14.Text = "C1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Location = new System.Drawing.Point(549, 424);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 13);
            this.label15.TabIndex = 59;
            this.label15.Text = "C2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Location = new System.Drawing.Point(659, 424);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 13);
            this.label16.TabIndex = 60;
            this.label16.Text = "C3";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Location = new System.Drawing.Point(205, 65);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 61;
            this.label17.Text = "Jugador 2";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label18.Location = new System.Drawing.Point(25, 61);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 62;
            this.label18.Text = "Jugador 1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btn_ver_c3_j2);
            this.Controls.Add(this.btn_ver_c2_j2);
            this.Controls.Add(this.btn_ver_c1_j2);
            this.Controls.Add(this.btn_ver_c3_j1);
            this.Controls.Add(this.btn_ver_c2_j1);
            this.Controls.Add(this.btn_ver_c1_j1);
            this.Controls.Add(this.btn_mazo_empate);
            this.Controls.Add(this.btn_mazo_j2);
            this.Controls.Add(this.btn_mazo_j1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_c3_j2);
            this.Controls.Add(this.btn_c2_j2);
            this.Controls.Add(this.btn_c1_j2);
            this.Controls.Add(this.btn_c3_j1);
            this.Controls.Add(this.btn_c2_j1);
            this.Controls.Add(this.valor_carta_j1);
            this.Controls.Add(this.btn_lista);
            this.Controls.Add(this.fotocarta);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Juego de Cartas";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ImageList listadeimagenes;
        private System.Windows.Forms.Label fotocarta;
        private System.Windows.Forms.Button btn_lista;
        private System.Windows.Forms.TextBox valor_carta_j1;
        private System.Windows.Forms.Button btn_c2_j1;
        private System.Windows.Forms.Button btn_c3_j1;
        private System.Windows.Forms.Button btn_c1_j2;
        private System.Windows.Forms.Button btn_c2_j2;
        private System.Windows.Forms.Button btn_c3_j2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_mazo_j1;
        private System.Windows.Forms.Button btn_mazo_j2;
        private System.Windows.Forms.Button btn_mazo_empate;
        private System.Windows.Forms.Button btn_ver_c1_j1;
        private System.Windows.Forms.Button btn_ver_c2_j1;
        private System.Windows.Forms.Button btn_ver_c3_j1;
        private System.Windows.Forms.Button btn_ver_c1_j2;
        private System.Windows.Forms.Button btn_ver_c2_j2;
        private System.Windows.Forms.Button btn_ver_c3_j2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
    }
}

