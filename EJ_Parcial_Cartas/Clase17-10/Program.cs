﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clase17_10
{
    
    class Program
    {
        struct carta
        {
            public int numero;
            public string palo;
            public bool estado;
        }
        static void Main(string[] args)
        {
            jugar();
        }

        static void jugar ()
        {
            int contadorcj1=0;
            int contadorcj2=0;
            bool salir = false;
            string op;
            carta[] mazo_de_cartas = new carta[48];
            generarmazo(mazo_de_cartas);
            dameunacarta(mazo_de_cartas);
            //for (int x = 0; x < 48; x++)
            //{
            //    Console.WriteLine(mazo_de_cartas[x].palo + " "+ mazo_de_cartas[x].numero + " " + "Estado: "+ mazo_de_cartas[x].estado);
            //}
            while (!salir)
            {
                Console.WriteLine("Queres una carta (Y/N): ");
                op = Console.ReadLine();
                op = op.ToUpper();
                if (op == "Y")
                {
                    clear();
                    carta carta_tmp_j1 = dameunacarta(mazo_de_cartas);
                    Console.WriteLine("Carta del Jugador 1: " + carta_tmp_j1.palo + " " + carta_tmp_j1.numero);


                    carta carta_tmp_j2 = dameunacarta(mazo_de_cartas);
                    Console.WriteLine("Carta del Jugador 2: " + carta_tmp_j2.palo + " " + carta_tmp_j2.numero);
                    if (carta_tmp_j1.numero> carta_tmp_j2.numero)
                    {
                        Console.WriteLine("El jugador 1 se lleva las cartas!!");
                        contadorcj1 += 2;
                    }
                    else if (carta_tmp_j2.numero > carta_tmp_j1.numero)
                    {
                        Console.WriteLine("El jugador 2 se lleva las cartas!!");
                        contadorcj2 += 2;
                    }
                    else if (carta_tmp_j1.numero==carta_tmp_j2.numero)
                    {
                        Console.WriteLine("Empate!");
                        contadorcj1++;
                        contadorcj2++;
                    }

                }
                else if (op == "N")
                {
                    salir = true;
                }
                clear();
                if (contadorcj1>contadorcj2)
                {
                    Console.WriteLine("El jugador 1 ha ganado la partida!!!");
                }
                else if (contadorcj2 > contadorcj1)
                {
                    Console.WriteLine("El jugador 2 ha ganado la partida!!!");
                }
                else if (contadorcj1 == contadorcj2)
                {
                    Console.WriteLine("Empate!!!");
                }
            }
            key();
        }

        static void generarmazo(carta[]mazo)
        {
            string[] palo = new string[4];
            palo[0] = "Oro";
            palo[1] = "Basto";
            palo[2] = "Espada";
            palo[3] = "Copa";
            string palotmp;
            int contadormazo = 0;
            for (int palo_int = 0; palo_int < 4; palo_int++)
            {
                palotmp = palo[palo_int];
                for (int carta_int = 1; carta_int < 13; carta_int++)
                {
                    carta carta = new carta();
                    carta.palo = palotmp;
                    carta.numero = carta_int;
                    carta.estado = false;
                    mazo[contadormazo] = carta;
                    contadormazo++;
                }
            }
            


        }

        static carta dameunacarta (carta [] mazo) // me devuelve un tipo de carta
        {
            bool salir = false;
            carta carta_temporal = new carta();
            Random rnd = new Random();

            do
            {
                int randomizador = rnd.Next(0, 48);
                if (mazo[randomizador].estado == false){ 
                    mazo[randomizador].estado = true;
                    carta_temporal = mazo[randomizador];
                }

            } while(!salir);


            return carta_temporal;
        }
        static void key ()
        {
            Console.ReadKey();
        }
        static void clear ()
        {
            Console.Clear();
        }

    }
}
